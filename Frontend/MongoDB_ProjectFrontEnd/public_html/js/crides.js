/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const host_base = "http://mongocommerce.tk:8181/api/";
//const host_base = "http://server.ungarmax.tk:8181/api/";

async function api(url, data){
    await enviarBsonServidor(url, data);
}

function enviarBsonServidor(url, data){
    return new Promise( (resolve, reject) => {
        let tipus;
        (data) ? tipus='POST' : tipus='GET';
        let httpxml = new XMLHttpRequest();
        let urlCompleta = host_base+url;
        httpxml.open(tipus, urlCompleta, true);
        httpxml.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
//        httpxml.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpxml.onload = function() {
            if (httpxml.readyState === 4) {
                if (httpxml.status === 200) {
                    resolve(httpxml.responseText);
                } else {
                    alert('Hi ha hagut un problema amb la sol·licitud.');
                    reject();
                }
            }
        };
        httpxml.onerror = function() {
            alert('Hi ha hagut un problema amb la xarxa.');
            reject();
        };
        console.log(data);
        console.log((data) ? JSON.stringify(data) : null);
        httpxml.send((data) ? JSON.stringify(data) : null);
//        httpxml.send((data) ? data : null);
    });
}


export default {
    api : enviarBsonServidor
}