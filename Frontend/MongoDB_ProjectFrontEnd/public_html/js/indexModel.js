"use strict"

import crides from './crides.js';

function IndexModel(){
    var self = this;
    self.esMostrenProductes = ko.observable(false);
    
    self.marques = ko.observableArray([]);
    self.categories = ko.observableArray([]);
    self.sexe = [{id:"hombre", name:"Hombre", check: false}, {id:"mujer", name:"Mujer", check: false}, {id:"unisex", name:"Unisex", check:false}];
    
    self.productes = ko.observableArray([]);
   
    self.productesFiltrats = ko.observableArray();
    
    self.marquesFiltrades = ko.observableArray();
    self.sexeFiltrat = ko.observableArray();
    self.categoriesFiltrades = ko.observableArray();
    self.categoriesTipusValorsFiltres = ko.observableArray();
    
    self.producteSelec = ko.observable();
    
    self.nouProdNom = ko.observable();
    self.nouProdMarca = ko.observable();
    self.nouProdCategoria = ko.observable();
    self.nouProdPreu = ko.observable();
    self.nouProdSexe = ko.observable();
    
    var determinatsArticlesActuals = ko.observable();
    
 
    self.amagarProductes = function(){
        self.esMostrenProductes(false);
        self.productes([]);
        self.productesFiltrats([]);
        self.marques([]);
        self.categories([]);
        self.marquesFiltrades([]);
        self.categoriesFiltrades([]);
    };
    
    
    function obtenerObj(text){
        return JSON.parse(text);
    }
    
    self.carregarProductes = async function(){
        
        let productsText = await crides.api("products");
        self.productes(obtenerObj(productsText));
        console.log(self.productes());

        self.productesFiltrats(self.productes().items);
        
        let marquesText = await crides.api("brands");
        self.marques();
        obtenerObj(marquesText).forEach(function(marca){
            self.marques.push({id: marca.id, name: marca.name, check: false});
        });
        console.log(self.marques());
        
        
        let categoriesText = await crides.api("types");
        obtenerObj(categoriesText).forEach(function(cat){
            console.log(cat);
            self.categories.push({id: cat.id, name: cat.name, filters: cat.filters, check: ko.observable(false)});
        });
        console.log(self.categories());
        
        
        self.esMostrenProductes(true);
    };
    
    self.filtrePerMarca = function(info){
        (info.check) ? self.marquesFiltrades.push(info.id) : self.marquesFiltrades.remove(info.id);
        actualitzarProductesFiltrats();
    };
    
    self.filtrePerSexe = function(info){
        (info.check) ? self.sexeFiltrat.push(info.id) : self.sexeFiltrat.remove(info.id);
        actualitzarProductesFiltrats();
    };
    
    self.filtrePerCategoria = async function(info){
        if(info.check()){
            if(self.categoriesFiltrades().length>0){
                let cat = self.categoriesFiltrades().pop();
                console.log(cat);
                
                console.log(self.categoriesTipusValorsFiltres());
                self.categories().forEach(function(info){
                    console.log(info);
                    if(info.id===cat) info.check(false);
                });
            }
            self.categoriesFiltrades.push(info.id);
            console.log(info.id);
            let valors = await crides.api('types/'+info.id);
//            self.categoriesTipusValorsFiltres(obtenerObj(valors));
//            console.log(self.categoriesTipusValorsFiltres());
            
            
            
            let prova = obtenerObj(valors);
            let arrayPare = [];
            prova.filters.forEach(function(info){
                let nom = info.filter;
                let array = [];
                info.values.forEach(function(infoFill){
                    array.push({valor: infoFill});
                });
                arrayPare.push({name: nom, valors: array, valorSeleccionat: ko.observable("")});
            });
            console.log(arrayPare);
            prova.filters=arrayPare;
            
            
            self.categoriesTipusValorsFiltres(prova);
            console.log(self.categoriesTipusValorsFiltres());
        } else{
            self.categoriesFiltrades.remove(info.id);
            console.log(self.categoriesFiltrades());    
        }
        actualitzarProductesFiltrats();
    };
    
    self.filtrePerCategoriaTipus = async function(info){
        console.log(info);
        if(info.valorSeleccionat()!== 'undefined'){
            let determinatsArticles = determinatsArticlesActuals();
            determinatsArticles += ("&attrs."+info.name+"="+info.valorSeleccionat());
            console.log(determinatsArticles);
            let productesFiltrats = await crides.api('products/'+determinatsArticles);
            (productesFiltrats) ? self.productesFiltrats(obtenerObj(productesFiltrats).items) : self.productesFiltrats(self.productes().items);
        }
    };
    
    async function actualitzarProductesFiltrats() {
        
        console.log(self.categories());
        console.log(self.categoriesFiltrades());
        
        
        
        let marquesFilt = (self.marquesFiltrades().length<=1) ? 'brand' : 'brand[]';
        let determinatsArticles = '?';
        self.marquesFiltrades().forEach(function(marca){
            if(determinatsArticles !== '?') determinatsArticles += '&';
            determinatsArticles += marquesFilt + '=' + marca;
        });
        if (self.categoriesFiltrades().length>0){
            let categoriesFilt = (self.categoriesFiltrades().length<=1) ? 'type' : 'type[]';
            self.categoriesFiltrades().forEach(function(categoria){
                if(determinatsArticles !== '?') determinatsArticles += '&';
                determinatsArticles += categoriesFilt + '=' + categoria;
            });
        }
        if(self.sexeFiltrat().length>0){
            let sexeFilt = (self.sexeFiltrat().length<=1) ? 'sex' : 'sex[]';
            self.sexeFiltrat().forEach(function(sexe){
                if(determinatsArticles !== '?') determinatsArticles += '&';
                determinatsArticles += sexeFilt + '=' + sexe;
            });
        }
        determinatsArticlesActuals(determinatsArticles);
        let productesFiltrats = await crides.api('products/'+determinatsArticles);
        (productesFiltrats) ? self.productesFiltrats(obtenerObj(productesFiltrats).items) : self.productesFiltrats(self.productes().items);
    }
    
    self.mostrarDetalls = function(data){
        self.producteSelec(data);
        $('#detallsProd').modal('show');
    };
    
    self.afegirProducte = function(){
        $('#afegirProd').modal('show');
    };
    
    self.guardarAfegirProd = async function(){
        if(self.nouProdCategoria() && self.nouProdMarca() &&
                self.nouProdNom() && self.nouProdPreu() && self.nouProdSexe()){
            var obj = {name: self.nouProdNom(), type:(self.nouProdCategoria().id).toLowerCase(), sex: self.nouProdSexe(), brand: (self.nouProdMarca()).toLowerCase(), 
                 price: parseFloat(self.nouProdPreu()), assets: { img: {src: "http://mongocommerce.tk:8181/images/a4a328a3-ed3b-4433-9b18-52d79e3ce304.jpg"}}, 
                 variants: [], attrs: {}};  

            await crides.api('products', obj);
            
            let productsText = await crides.api("products");
            self.productes(obtenerObj(productsText));
            actualitzarProductesFiltrats();
            self.cancelarAfegirProd();
        } else {
            var myToast = Toastify({
                text: "Error! Por favor rellene todos los campos!",
                duration: 2000
            });
            myToast.showToast();
        }
    };
    
    self.prova = function(info){
        console.log(info);
    };
    
    self.mostrarSubcategoria = function(info){
        return info.name;
    };
    
    self.cancelarAfegirProd = function(){
       $('#afegirProd').modal('hide');
        self.nouProdNom(null);
        self.nouProdMarca(null);
        self.nouProdCategoria(null);
        self.nouProdPreu(null);
    };
    
    
//    self.filtrarPerCategoria = function(data){
//        console.log(data);
//        self.productes().forEach(function(prod){
//           if(data===prod.categoria) self.productesFiltrats.push(prod);
//        });
//    };
    
    
}

ko.applyBindings(new IndexModel());



