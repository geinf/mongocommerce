# MongoCommerce

  An e-commerce platform built with MongoDB and NodeJS.
  
## Instal·lació

Primer de tot, [descarrega i instala Node.js](https://nodejs.org/es/download/).

A continuació, cal instal·lar les dependències del projecte, que es fa amb la següent comanda següent que s'ha d'executar estant situats a la carpeta arrel del projecte:
```bash
$ npm install
```

Un cop això tindrem les dependències del projecte instal·lades.

Seguidament caldrà especificar els valors de la configuració, propis de cada usuari i entorn. Els valors de configuració són els següents: port d'escolta de l'API, dades de login i base de dades de MongoDB.

Per fer això caldrà reanomenar el fitxer `example.env` a `.env`

Aquest fitxer té el següent format:
```shell
# Configuració API
API_PORT=8181

# Dades MongoDB
MONGODB_AUTH_USER=user
MONGODB_AUTH_PASSWORD=password
MONGODB_AUTH_DB=admin
MONGODB_DB=database
```

Finalment, situats a la carpeta arrel del projecte, executem la següent comanda per tal d'iniciar el servidor i que es quedi a l'espera de peticions:
```bash
$ npm start
```

## Directoris

* **lib**: fitxers codi que s'utilitzen al projecte com a llibreries o mòduls aïllats.
* **src**: fitxers codi que formen part del projecte.
    * **api**: allò relacionat amb el codi de les rutes de l'API.
        * **xyz**: allò relacionat amb la ruta `/xyz`
            * ***xyz.get.js***: codi corresponent al mètode GET sobre la ruta `/xyz`
        * ***index.js***: codi que agrupa i registra totes les rutes i mètodes per afegir-les al Router.
    * **db**: allò relacionat amb la base de dades.
    * ***app.js***: punt d'entrada de l'aplicació.