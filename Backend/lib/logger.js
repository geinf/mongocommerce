#!/usr/bin/env node

const Logger = function(opts) {
    'use strict';

    const SimpleLogger = require('simple-node-logger');
	const manager = new SimpleLogger();
	let appender = manager.createConsoleAppender(opts);

	const chalk = require('chalk');

	const colors = {
	  trace  : (text) => chalk.blueBright(text),
	  debug  : (text) => chalk.white(text),
	  info   : (text) => chalk.cyanBright(text),
	  success: (text) => chalk.greenBright(text),
	  warn   : (text) => chalk.yellowBright(text),
	  error  : (text) => chalk.red(text),
	  fatal  : (text) => chalk.red.inverse(text)
	};

	appender.formatter = function(entry) {
	    const fields = appender.formatEntry(entry)
	    let colorize = colors[entry.level] || function(text) { return text }

	    return colorize(fields.join(appender.separator));
	};

	manager.createFileAppender(opts);

	let log = manager.createLogger();

	if ("level" in opts)
		log.setLevel(opts["level"]);

	return log;
};

module.exports = Logger;