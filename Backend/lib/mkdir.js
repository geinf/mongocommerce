#!/usr/bin/env node

const fs = require('fs');

function mkdir(targetDir)
{
	if (!fs.existsSync(targetDir)) // Si el directori especificat no existeix
		fs.mkdirSync(targetDir, { recursive: true }); // Crear-lo recursivament (si falten directoris pare es creen)
}

module.exports = mkdir;