#!/usr/bin/env node

const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const ObjectId = mongo.ObjectID;
const QueryString = require('mongo-querystring');
const queryParser = new QueryString({
	blacklist: { // No considerar com a part de la query Mongo
		skip: 'skip', sort: 'sort'
	},
	custom: {
		brand: function(query, input) {
			if (input instanceof Array) // Passen més d'una marca
				query['brand_internal'] = { $in: input };

			else // Només passen una marca
				query['brand_internal'] = input;
		},
		type: function(query, input) {
			if (input instanceof Array) // Passen més d'un tipus
				query['type_internal'] = { $in: input };

			else // Només passen un tipus
				query['type_internal'] = input;
		}
	}
});

class MongoDB
{
	constructor(address, port, username, password, database)
	{
		this.url = "mongodb://" + encodeURIComponent(username) + ":" + encodeURIComponent(password);
		this.url += "@" + address + ":" + port;
		this.url += "/?authMechanism=DEFAULT&authSource=admin";
		this.database = database;
	}

	connect()
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			MongoClient.connect(instance.url, { useNewUrlParser: true }, function (err, client) {
				if (err) // Error establint connexió amb la base de dades
					reject(err);

				else // Connexió amb la base de dades correcta
				{
					log.debug('Database connection successfully established.');
					instance.connection = client;
					resolve(true);
				}
			});
		});
	}

	isConnected()
	{
		return (this.connection != undefined);
	}

	disconnect()
	{
		if (this.connection != null)
			this.connection.close();

		return true;
	}

	getProduct(id)
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('products')
				.findOne({ _id: new ObjectId(id) }, function(err, result) {
					if (err) // Error amb la base de dades
						reject(-2, err);

					else if (result == null) // Sense resultats
						reject(-1, null);

					else // Producte trobat
						resolve(result);
				});
		});
	}

	getProducts(params, skip, sort)
	{
		var instance = this;
		var query = queryParser.parse(params); // Conversió dels paràmetres a query MongoDB
		var offset = skip ? parseInt(skip) : 0; // Si no s'ha especificat cap offset, agafem 0; altrament, el propi offset

		const sortCriteria = {}; // Objecte que contindrà els criteris d'ordenació

		if (sort) // Si s'ha especificat ordenació explícita
		{
			if (sort.includes(';')) // L'ordenació és de vàries columnes
			{
				sort.split(';').forEach(function(entry) { // Processem les ordenacions
					let splitted = entry.split(','); // Separem columna de criteri (ascendent/descendent)
					sortCriteria[splitted[0]] = parseInt(splitted[1]);
				});
			}

			else // Només és ordenació d'una columna
			{
				let splitted = sort.split(','); // Separem columna de criteri (ascendent/descendent)
				sortCriteria[splitted[0]] = parseInt(splitted[1]);
			}
		}

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('products')
				.find(query) // Query
				.skip(offset) // Offset especificat
				.limit(25) // Límit de 25 resultats
				.sort(sortCriteria) // Ordenació segons criteris
				.toArray(function(err, result) {
					if (err) // Error amb la base de dades
						reject(err);

					else // Consulta correcta
						resolve(result);
				});
		});
	}

	getTypes()
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('types')
				.find({}) // Només obtenir el nom de totes les marques (sense la ID)
				.project({ _id: 0 }) // No mostrar la columna _id
				.sort({ id: 1 }) // Ordenació ascendent per id
				.toArray(function(err, result) {
					if (err) // Error amb la base de dades
						reject(err);

					else // Consulta correcta
						resolve(result);
				});
		});
	}

	getBrands()
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('brands')
				.find({}) // Obtenir totes les marques
				.project({ _id: 0 }) // No mostrar la columna _id
				.sort({ id: 1 }) // Ordenació ascendent per id
				.toArray(function(err, result) {
					if (err) // Error amb la base de dades
						reject(err);

					else // Consulta correcta
						resolve(result);
				});
		});
	}

	getType(id)
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('types')
				.findOne({ id: id }, function(err, result) {
					if (err) // Error amb la base de dades
						reject(-2, err);

					else if (result == null) // Sense resultats
						reject(-1, null);

					else // Tipus trobat
						resolve(result);
				});
		});
	}

	getBrand(id)
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('brands')
				.findOne({ id: id }, function(err, result) {
					if (err) // Error amb la base de dades
						reject(-2, err);

					else if (result == null) // Sense resultats
						reject(-1, null);

					else // Marca trobat
						resolve(result);
				});
		});
	}

	createProduct(product)
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('products')
				.insertOne(product, function(err, result) {
					if (err) // Error amb la base de dades
						reject(err);

					else // Inserció correcta
						resolve(result.ops[0]);
				});
		});
	}

	distinctValuesForColumn(column, type)
	{
		var instance = this;

		return new Promise(function(resolve, reject)
		{
			instance.connection.db(instance.database).collection('products')
				.distinct(
					'attrs.' + column,
					{ type_internal: type },
					function(err, result) {
						if (err) // Error amb la base de dades
							reject(err);

						else // Consulta correcta
							resolve(result);
					}
				);
		});
	}

	distinctValuesForColumns(columns, type)
	{
		var instance = this;
		var promises = [];

		for (let column of columns)
			promises.push(instance.distinctValuesForColumn(column, type));

		return Promise.all(promises);
	}
};

module.exports = MongoDB;