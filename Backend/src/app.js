#!/usr/bin/env node

require('dotenv').load();

const Logger = require('../lib/logger.js');
const opts = {
	level: 'all', // Registrar tots els nivells de log
	logFilePath: 'app.log', // Ruta fitxer log
	timestampFormat: 'DD-MM-YYYY HH:mm:ss.SSS' // Format de les marques de temps
};
log = new Logger(opts); // Variable global "log" per permetre fer crides des d'altres fitxers

const express    = require('express'); // API Rest
const app        = express();
const bodyParser = require('body-parser'); // Tractament de dades d'un POST
const fixCors	 = require('../lib/cors.js'); // Correció de l'error CORS
const imageUpload = require('express-fileupload');

halson = require('halson'); // Format HALSON (http://stateless.co/hal_specification.html)

const MongoDB = require('./db/mongodb.js');
mongo = new MongoDB(
	process.env.MONGODB_SERVER,
	process.env.MONGODB_PORT,
	process.env.MONGODB_USER,
	process.env.MONGODB_PASSWORD,
	process.env.MONGODB_DATABASE
);

// Creació de la carpeta /public/img
const mkdir = require('../lib/mkdir.js');
mkdir('./public/img');

mongo.connect().then(function(result) {
	// Corregir error CORS
	app.use(fixCors);

	// Utilizar bodyParser per tractar dades dels POST en format JSON
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());

	// Per poder penjar imatges
	app.use(imageUpload());

	// Port d'escolta
	const port = process.env.API_PORT || 8080;

	// Obtenir instància del Router
	const router = require('./api/index.js');

	// Registrar les rutes de "router" amb la URL base
	app.use('/api', router);

	// Servir les imatges del directori public/img
	app.use('/images', express.static('public/img'));

	// Iniciar servidor
	var server = app.listen(port);

	log.info('Listening on port ' + port);

	// Tractament interrupció Control+C
	process.on('SIGINT', () => {
		server.close();
		mongo.disconnect();
		process.exit(0);
	});
})
.catch(function(err) {
	log.error('Could not establish connection with the database. (' + err + ')');
});