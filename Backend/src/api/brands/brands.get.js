module.exports = function(req, res) {
	mongo.getBrands().then(function(result) {
		res.status(200);
		res.setHeader('Content-Type', 'application/json');
		res.send(JSON.stringify(result));
	})
	.catch(function(err, msg) {
		res.status(500); // Error 500: Internal Server Error
		res.setHeader('Content-Type', 'application/vnd.error+json');
		res.json({ message: 'Failed to fetch brands' });

		log.error(err);
	})
	.finally(function() {
		log.debug('Served GET /brands request');
	});
}