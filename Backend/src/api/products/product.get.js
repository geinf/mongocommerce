module.exports = function(req, res) {
	mongo.getProduct(req.params.id).then(function(result) {
	    res.status(200);
	    res.setHeader('Content-Type', 'application/hal+json');
	    res.send(JSON.stringify(result));
	})
	.catch(function(err, msg) {
		if (err == -1) { // Not found
			res.status(404); // Error 404: Not Found
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to find product with specified id' });
		}

		else if (err == -2) { // Database error
			res.status(500); // Error 500: Internal Server Error
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to fetch results' });

    		log.error(msg);
		}
	})
	.finally(function() {
		log.debug('Served GET /products/' + req.params.id + ' request');
	});
}