module.exports = function(req, res) {
	mongo.getProducts(req.query, req.query.skip, req.query.sort).then(function(result) {
		res.status(200);
		res.setHeader('Content-Type', 'application/hal+json');

		let baseLink = req.path;

		let selfLink = '';
		let nextLink = '';

		if (Object.keys(req.query).length) {
			selfLink = baseLink + '&';
			nextLink = baseLink + '&';
		}

		else {
			selfLink = baseLink + '?';
			nextLink = baseLink + '?';
		}

		selfLink += 'skip=' + (req.query.skip || 0);
		nextLink += 'skip=' + (Number(req.query.skip) + 25 || 25);

		var response = halson({ items: result })
		.addLink('self', selfLink)
		.addLink('next', nextLink);

		res.send(JSON.stringify(response));
	})
	.catch(function(err, msg) {
		res.status(500); // Error 500: Internal Server Error
		res.setHeader('Content-Type', 'application/vnd.error+json');
		res.json({ message: 'Failed to fetch results' });

		log.error(err);
	})
	.finally(function() {
		log.debug('Served GET /products/' + JSON.stringify(req.query) + ' request');
	});
}