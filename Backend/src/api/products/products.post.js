module.exports = function(req, res) {
	var resource = req.body;
	var error = false;

	mongo.getType(resource.type).then(function(result) {
		delete resource.type;
		resource.type_internal = result.id;
		resource.type = result.name;
	})
	.then(function() { return mongo.getBrand(resource.brand) })
	.then(function(result) {
		delete resource.brand;
		resource.brand_internal = result.id;
		resource.brand = result.name;
	})
	.then(function() { return mongo.createProduct(resource) })
	.then(function(result) {
		res.status(200);
		res.setHeader('Content-Type', 'application/hal+json');
		res.send(JSON.stringify(result));

		product = result;
	})
	.catch(function(err, msg) {
		error = true;

		if (err == -1) { // Not found
			res.status(400); // Error 400: Bad Request
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to find brand/type' });
		}

		else if (err == -2) { // Database error
			res.status(500); // Error 500: Internal Server Error
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to fetch brand/type' });

    		log.error(msg);
		}

		else if (err.code == 121) { // 121: error estructural del producte rebut
			res.status(400); // Error 400: Bad Request
			res.setHeader('Content-Type', 'application/vnd.error+json');
			res.json({ message: 'Invalid schema' });
		}

		else {
			res.status(500); // Error 500: Internal Server Error
			res.setHeader('Content-Type', 'application/vnd.error+json');
			res.json({ message: 'Failed to create product' });

			log.error(err);
		}
	})
	.finally(function() {
		log.debug('Served POST /products request');

		if (!error)
			log.info('Product with ID ' + product._id + ' successfully created');
	});
}