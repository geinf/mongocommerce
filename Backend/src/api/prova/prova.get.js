module.exports = function(req, res) {
	mongo.test().then(function(result) {
	    res.status(200);
	    res.setHeader('Content-Type', 'application/hal+json');

	    var resource = halson({
			product: result
	    })
	    .addLink('self', '/product_quantities/')
	    .addLink('next', '/2/');

	    res.send(JSON.stringify(resource));
	})
	.catch(function(err) {
		res.status(500); // Error 500: Internal Server Error
	    res.setHeader('Content-Type', 'application/vnd.error+json');
	    res.json({ message: 'Failed to fetch results' });
	})
	.finally(function() {
		log.info('Served GET /prova request');
	});
}