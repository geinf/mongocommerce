#!/usr/bin/env node

const express    = require('express'); // API Rest

const getProducts = require('./products/products.get.js');
const getProduct = require('./products/product.get.js');
const postProduct = require('./products/products.post.js');

const getBrands = require('./brands/brands.get.js');

const getTypes = require('./types/types.get.js');
const getType = require('./types/type.get.js');

const postImage = require('./images/images.post.js');

const provaGet 	 = require('./prova/prova.get.js');

var router = express.Router();

router.get('/products', getProducts);
router.get('/products/:id', getProduct);
router.post('/products', postProduct);

router.get('/brands', getBrands);

router.get('/types', getTypes);
router.get('/types/:id', getType);

router.post('/images', postImage);

router.get('/prova', provaGet);

module.exports = router;