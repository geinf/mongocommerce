const uuid = require('uuid/v4');

module.exports = function(req, res) {
	if (!('files' in req) || Object.keys(req.files).length == 0 || !('image' in req.files)) {
		return res.status(400).send({ message: 'No files were uploaded' });
	}

	// Fitxer pujat
	let image = req.files.image;

	// Prohibir altres fitxers que no siguin imatges
	if (!image.mimetype.startsWith('image/')) {
		log.warn('User tried to upload a file that was not an image');
		return res.status(403).send({ message: 'Only images allowed' });
	}

	// Generació d'un nom aleatori
	let name = uuid();

	// Extensió del fitxer pujat
	let extension = '.' + image.name.split('.').pop();

	// Use the mv() method to place the file somewhere on your server
	image.mv(process.cwd() + '/public/img/' + name + extension, function(err) {
		if (err) {
			res.status(500); // Error 500: Internal Server Error
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to upload image' });

    		log.error(err);
		}

		else {
			log.info('Image uploaded (' + name + extension + ')');

			res.status(200);
			res.send(name + extension);
		}
	});
}