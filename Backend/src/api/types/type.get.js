module.exports = function(req, res) {
	var resource;
	var filters = [];

	mongo.getType(req.params.id).then(function(result) {
		resource = result;
		filters = result.filters;
		return mongo.distinctValuesForColumns(result.filters, req.params.id);
	})
	.then(function(result) {
		delete resource.filters;
		resource.filters = [];

		for (let i = 0; i < result.length; i++)
			resource.filters.push({ filter: filters[i], values: result[i] });

		res.status(200);
	    res.setHeader('Content-Type', 'application/hal+json');
	    res.send(JSON.stringify(resource));
	})
	.catch(function(err, msg) {
		if (err == -1) { // Not found
			res.status(404); // Error 404: Not Found
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to find type with specified id' });
		}

		else { // Database error
			res.status(500); // Error 500: Internal Server Error
	    	res.setHeader('Content-Type', 'application/vnd.error+json');
	    	res.json({ message: 'Failed to fetch results' });

    		log.error(err);
		}
	})
	.finally(function() {
		log.debug('Served GET /types/' + req.params.id + ' request');
	});
}