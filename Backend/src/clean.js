#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const directory = process.cwd() + '/public/img';

console.log('Cleaning application log...');

fs.unlink(path.join(process.cwd(), 'app.log'), err => {
	if (err) throw err;
});

console.log('Application log successfully cleaned.');

console.log('Cleaning images folder...');

fs.readdir(directory, (err, files) => {
	if (err) throw err;

	for (const file of files) {
		fs.unlink(path.join(directory, file), err => {
			if (err) throw err;
		});
	}

	console.log('Images folder successfully cleaned.');
});